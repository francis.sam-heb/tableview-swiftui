//
//  RefundReason.swift
//  MyHEB
//
//  Created by Francis,Samuel on 3/15/21.
//  Copyright © 2021 H-E-B. All rights reserved.
//

/// Represents a reason that a customer may want a refund for a product
struct RefundReason {
    /// An ID/enumeration for the reason. Could come from the server. TBD.
    let id: String
    /// Text to be displayed to the user.
    let displayName: String
    /// Whether or not the reason requires further details from the user.
    let detailsRequired: Bool
}

/// Represents the model for a tag to be displayed in a list of refund reasons
struct RefundReasonTag: Tag {
    var id: String { refundReason.id }
    var displayText: String {
        String(format: "%d %@",
               quantity,
               refundReason.displayName)
    }

    /// The reason instance associated with this tag
    private let refundReason: RefundReason
    /// The number of products that the customer is requesting refunds for this reason
    private let quantity: Int

    init(refundReason: RefundReason, quantity: Int) {
        self.refundReason = refundReason
        self.quantity = quantity
    }

    static let samples: [RefundReasonTag] = [
        ("missing", "Missing", 1),
        ("damaged", "Damaged", 653_434),
        ("expired", "Expired / spoiled", 23),
        ("other", "Other", 1),
        ("exploded", "Exploded", 3),
        ("ateDog", "Ate my dog", 8),
        ("hurtFeelings", "Hurt my feelings", -7),
        ("longAnswer", "Spent far too much time watching TV and playing video games", 15),
        ("tastedBad", "Tasted bad", 3)
    ]
    .map { id, displayName, quantity in
        RefundReasonTag(refundReason: RefundReason(id: id, displayName: displayName, detailsRequired: false),
                        quantity: quantity)
    }
}
