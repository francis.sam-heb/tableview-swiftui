//
//  Tag.swift
//  MyHEB
//
//  Created by Francis,Samuel on 3/10/21.
//  Copyright © 2021 H-E-B. All rights reserved.
//

/// Represents the model for a tag, which can be supplied to a `TagView`
protocol Tag: Identifiable {
    var displayText: String { get }
}
