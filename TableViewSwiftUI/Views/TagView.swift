//
//  TagView.swift
//  MyHEB
//
//  Created by Francis,Samuel on 3/10/21.
//  Copyright © 2021 H-E-B. All rights reserved.
//

import SwiftUI

struct TagView: View {
    private let text: String

    init(text: String) {
        self.text = text
    }

    var body: some View {
        Text(text)
            .foregroundColor(Color(.black))
            .font(.caption)
            .lineLimit(1)
            .padding(EdgeInsets(
                top: 4,
                leading: 12,
                bottom: 4,
                trailing: 12
            ))
            .background(Color(.lightGray))
            .clipShape(Capsule())
    }
}

struct TagView_Previews: PreviewProvider {
    static var previews: some View {
        TagView(text: "Hello")
    }
}
