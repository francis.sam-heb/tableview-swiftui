//
//  MyTableViewCell.swift
//  TableViewSwiftUI
//
//  Created by Francis,Samuel on 4/26/21.
//

import UIKit
import SwiftUI

class MyTableViewCell: UITableViewCell {
    @IBOutlet weak var stackView: UIStackView!
    private var tagListController: UIHostingController<TagListView<RefundReasonTag>>!

    override func awakeFromNib() {
        super.awakeFromNib()
        let tagListView = TagListView(tags: RefundReasonTag.samples)
        let tagListController = UIHostingController(rootView: tagListView)
        self.tagListController = tagListController
        guard let tagListUIView = tagListController.view else { return }
        tagListUIView.translatesAutoresizingMaskIntoConstraints = false
        tagListUIView.setContentHuggingPriority(.required, for: .vertical)
        tagListUIView.setContentCompressionResistancePriority(.required, for: .vertical)
//        stackView.addArrangedSubview(tagListUIView)
        contentView.addSubview(tagListUIView)
        NSLayoutConstraint.activate([
            tagListUIView.topAnchor.constraint(equalTo: contentView.topAnchor),
            tagListUIView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            tagListUIView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tagListUIView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
        ])
    }

    func configure(parentViewController: UIViewController) {
        tagListController.removeFromParent()
        parentViewController.addChild(tagListController)
        tagListController.didMove(toParent: parentViewController)
    }
}
