//
//  SizingView.swift
//  TableViewSwiftUI
//
//  Created by Francis,Samuel on 4/26/21.
//

import SwiftUI

struct SizingView<T: View>: View {
    let view: T
    @State var updateSizeHandler: ((_ size: CGSize) -> Void) = { size in
        print("Handler not set. Size: \(size)")
    } {
        didSet {
            print("did set")
        }
    }
    init(view: T) {
        self.view = view
    }

    var body: some View {
        view.background(
            GeometryReader { proxy in
                Color.clear
                    .preference(key: SizePreferenceKey.self, value: proxy.size)
            }
        )
        .onPreferenceChange(SizePreferenceKey.self) { preferences in
            updateSizeHandler(preferences)
        }
    }

    func size(with view: T, geometry: GeometryProxy) -> T {
        updateSizeHandler(geometry.size)
        return view
    }
}

struct SizePreferenceKey: PreferenceKey {
    typealias Value = CGSize

    static var defaultValue: CGSize = .zero

    static func reduce(value: inout CGSize, nextValue: () -> CGSize) {
        value = nextValue()
    }

}

//struct SizingView_Previews: PreviewProvider {
//    static var previews: some View {
//        SizingView(
//            view: TagListView(tags: RefundReasonTag.samples, updateSizeHandler: nil)
//        )
//    }
//}
