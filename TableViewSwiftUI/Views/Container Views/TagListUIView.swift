//
//  TagListUIView.swift
//  TableViewSwiftUI
//
//  Created by Francis,Samuel on 4/26/21.
//

import UIKit
import SwiftUI

class TagListUIView<TagType: Tag>: UIView {
    private var parentViewController: UIViewController
    private let tagListView: TagListView<TagType>
    private var tagListController: UIHostingController<TagListView<TagType>>!
    var heightConstraint: NSLayoutConstraint!

    init(tags: [TagType], parentViewController: UIViewController) {
        self.parentViewController = parentViewController
        self.tagListView = TagListView(tags: tags)
        super.init(frame: .zero)
        
        tagListController = UIHostingController(rootView: tagListView)
        guard let uiView = tagListController.view else { return }
        uiView.translatesAutoresizingMaskIntoConstraints = false
        parentViewController.addChild(tagListController)
        tagListController.didMove(toParent: parentViewController)

        addSubview(uiView)

        heightConstraint = uiView.heightAnchor.constraint(equalToConstant: 0)

        NSLayoutConstraint.activate([
            uiView.topAnchor.constraint(equalTo: topAnchor),
            uiView.bottomAnchor.constraint(equalTo: bottomAnchor),
            uiView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 28),
            uiView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -28)
        ])
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        tagListController.view.sizeToFit()
        let height = tagListController.view.bounds.height
        print("Height: \(height)")
        heightConstraint.constant = height
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
