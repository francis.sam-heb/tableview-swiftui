//
//  TagListView.swift
//  MyHEB
//
//  Created by Francis,Samuel on 3/10/21.
//  Copyright © 2021 H-E-B. All rights reserved.
//

import SwiftUI

struct TagListView<TagType: Tag>: View {
    let tags: [TagType]
    private let tagPadding: CGFloat = 4
    @State private var totalHeight: CGFloat

    init(tags: [TagType]) {
        self.tags = tags
        _totalHeight = State(initialValue: .infinity)
    }

    var body: some View {
        VStack {
            GeometryReader(content: generateContent(in:))
        }
        .frame(maxHeight: totalHeight)
    }

    private func generateContent(in geometry: GeometryProxy) -> some View {
        var width = CGFloat.zero
        var height = CGFloat.zero

        return ZStack(alignment: .topLeading) {
            ForEach(tags.indices, id: \.self) { index in
                let tag = tags[index]
                TagView(text: tag.displayText)
                    .alignmentGuide(.leading) { viewDimensions in
                        if abs(width - viewDimensions.width) > geometry.size.width {
                            width = .zero
                            height -= viewDimensions.height + tagPadding
                        }

                        let result = width
                        if index == tags.endIndex - 1 {
                            width = .zero // last item
                        } else {
                            width -= viewDimensions.width + tagPadding
                        }

                        return result
                    }
                    .alignmentGuide(.top) { _ in
                        let result = height
                        if index == tags.endIndex - 1 {
                            height = .zero // last item
                        }

                        return result
                    }
                    // Establishes readout order for VoiceOver mode. low index -> high priority
                    .accessibility(sortPriority: Double(tags.count - index))
            }
        }
        .accessibilityElement(children: .contain)
        .background(viewHeightReader($totalHeight))
    }

    private func viewHeightReader(_ binding: Binding<CGFloat>) -> some View {
      return GeometryReader { geo -> Color in
        DispatchQueue.main.async {
          binding.wrappedValue = geo.frame(in: .local).size.width
        }
        return .clear
      }
    }
}

struct TagListView_Previews: PreviewProvider {
    private static let sampleTags: [RefundReasonTag] = [
        ("missing", "Missing", 1),
        ("damaged", "Damaged", 653_434),
        ("expired", "Expired / spoiled", 23),
        ("other", "Other", 1),
        ("exploded", "Exploded", 3),
        ("ateDog", "Ate my dog", 8),
        ("hurtFeelings", "Hurt my feelings", -7),
        ("longAnswer", "Spent far too much time watching TV and playing video games", 15),
        ("tastedBad", "Tasted bad", 3)
    ]
    .map { id, displayName, quantity in
        RefundReasonTag(refundReason: RefundReason(id: id, displayName: displayName, detailsRequired: false),
                        quantity: quantity)
    }

    static var previews: some View {
        TagListView(tags: Self.sampleTags)
            .previewDevice("iPhone 12 mini")
    }
}
